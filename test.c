#include <assert.h>
#include <stdio.h>
#include <dlfcn.h>

typedef void (*fun)(void);
typedef void (*call)(void*);
typedef int (*store)(int);

int main(int argc, char* argv[])
{
	void *lib1 = dlopen("./lib1.so", RTLD_NOW);
	assert(lib1);

	void *lib2 = dlopen("./lib2.so", RTLD_NOW);
	assert(lib2);

	fun fun = dlsym(lib1, "fun");
	assert(fun);

	call call = dlsym(lib2, "call");
	assert(call);

	store store1 = dlsym(lib1, "store");
	assert(store1);
	assert(store1(42) == 1);
	assert(store1(43) == 2);

	store store2 = dlsym(lib2, "store");
	assert(store2);
	assert(store2(42) == 1);
	assert(store2(43) == 2);

	printf("fun: %p\n", fun);
	fflush(stdout);

	call(fun);

	dlclose(lib1);
	dlclose(lib2);
	return 0;
}
