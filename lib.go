package main

// inline void call2(void *p)
// {
//   void (*f)(void) = p;
//   f();
// }
import "C"
import (
	"fmt"
	"unsafe"
)

var storage []int

func main() {
}

//export fun
func fun() {
	fmt.Println("fun!")
	panic("fun!")
}

//export call
func call(p unsafe.Pointer) {
	fmt.Printf("calling %p\n", p)
	C.call2(p)
}

//export store
func store(n C.int) C.int {
	storage = append(storage, int(n))
	return C.int(len(storage))
}
