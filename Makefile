GNUMAKE ?= gmake

all:
	@${GNUMAKE} $@

.DEFAULT:
	@${GNUMAKE} $@
